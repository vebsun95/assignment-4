import applyForLoan from "../DomainLogic/applyForLoan.js";

function getALoanBtnHandler(state)
{
    let enteredLoanAmount = prompt("Enter loan amount: ");
    let loanAmout = Number.parseInt(enteredLoanAmount);
    if(isNaN(loanAmout))
        return; 
    const currentBalance = state.balance;
    const currentLoan = state.loan;
    if(!applyForLoan(currentBalance, currentLoan, loanAmout)) {
        alert(`Your application for loan has been denied`);
        return;
    }
    return {
        balance: currentBalance + loanAmout,
        loan: currentLoan + loanAmout,
    }
}

export default getALoanBtnHandler;