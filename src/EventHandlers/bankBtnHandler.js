import transferSalaryToBank from "../DomainLogic/transferSalaryToBank.js";

function bankBtnHandler(state)
{
    const currentSalary = state.salary;
    const currentLoan = state.loan;
    const currentBalance = state.balance;
    const [newLoan, newBalance] = transferSalaryToBank(currentLoan, currentBalance, currentSalary);
    return {
        salary: 0,
        loan: newLoan,
        balance: newBalance,
    };
}

export default bankBtnHandler;