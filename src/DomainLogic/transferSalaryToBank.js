function transferSalaryToBank(currentLoan, currentBalance, currentSalary)
{
    let newLoan = 0;
    let newBalance = currentBalance + currentSalary;
    if(currentLoan > 0)
    {
        newLoan = currentLoan - currentSalary * 0.1;
        newBalance = currentBalance + currentSalary * 0.9;
        if(newLoan < 0) {
            newLoan = 0;
            newBalance = currentBalance + currentSalary - currentLoan;
        }
    }
    return [newLoan, newBalance]
}

export default transferSalaryToBank;