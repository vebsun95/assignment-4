import Component from "../Core/Component.js";

export const fireworkElementIds = {
    modalBg: 'modal-bg',
}

class Fireworks extends Component {
    #modalBgElement = document.getElementById("modal-bg");
    #fireworkElements = document.getElementsByClassName("firework");
    #fireworkTextElement = document.getElementById("firework-text");

    constructor(appCallback) {
        super(appCallback);

        this.#modalBgElement.onclick = appCallback;
    }

    init() {
        this.#modalBgElement.style.display = "none";
    }

    update(state) {
        let {komputer, showFireworks, closeModal} = state;
        if(closeModal === true) {
            this.#modalBgElement.style.display = "none";
        }
        if(showFireworks === true) {
            this.#modalBgElement.style.display = "block";
            let komputerNameElement = this.#fireworkTextElement.querySelector("span")
            komputerNameElement.innerText = komputer.title;
        }
    }

}

export default Fireworks;