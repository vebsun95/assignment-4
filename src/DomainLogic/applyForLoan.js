function applyForLoan(currentBalance, currentLoan, loanAmout)
{
    if(currentLoan > 0)
        return false;
    if(currentBalance * 2 < loanAmout)
        return false;
    return true;
}

export default applyForLoan;