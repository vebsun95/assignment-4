function closeModalHandler(state) {
    return {
        closeModal: true,
        showFireworks: false,
    }
}

export default closeModalHandler;