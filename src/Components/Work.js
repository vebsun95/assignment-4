import Component from "../Core/Component.js";
import animateNumber from "../Utils/animateNumber.js";

export const workElementIds = {
    bankBtn: 'bank-btn',
    workBtn: 'work-btn',
    repayBtn: 'repay-loan-btn',
    salary: 'salary-amount-span',
};

class Work extends Component {
    #bankBtnElement =  document.getElementById(workElementIds.bankBtn);
    #workBtnElement =  document.getElementById(workElementIds.workBtn);
    #repayBtn       = document.getElementById(workElementIds.repayBtn);
    #salaryElement =  document.getElementById(workElementIds.salary);
    constructor(appCallback)
    {
        super(appCallback);

        if(this.#bankBtnElement == null) throw new Error('No HTML element with id: ' + workElementIds.bankBtn);
        if(this.#workBtnElement == null) throw new Error('No HTML element with id: ' + workElementIds.workBtn);
        if(this.#repayBtn == null) throw new Error('No HTML element with id: ' + workElementIds.repayBtn);
        if(this.#salaryElement  == null) throw new Error('No HTML element with id: ' + workElementIds.salary);
        
        this.#bankBtnElement.onclick = appCallback;
        this.#workBtnElement.onclick = appCallback;
        this.#repayBtn.onclick = appCallback;
    }


    init() {
        this.#repayBtn.style.display = 'none';
    }

    update(state) {
        this.render(state);
    }

    render(state) {
        const { oldSalary, salary, loan } = state;
        animateNumber(this.#salaryElement, oldSalary, salary);
        loan < 1 ? this.#repayBtnInvisible() : this.#repayBtnVisible(); 
    }

    #repayBtnVisible() {
        this.#repayBtn.style.display = "unset";
    }

    #repayBtnInvisible() {
        this.#repayBtn.style.display = "none";
    }
}


export default Work;