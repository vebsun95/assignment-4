import Component from "../Core/Component.js";

export const browseElementIds = {
    selectElement: 'laptop-select-dropdown',
    specContainer: 'laptop-specs',
};

class Browse extends Component {
    #selectElement = document.getElementById(browseElementIds.selectElement);
    #specContainerElement = document.getElementById(browseElementIds.specContainer);

    constructor(appCallback) {
        super(appCallback);

        if (this.#selectElement == null) throw new Error('No HTML element with id: ' + browseElementIds.selectElement);
        if (this.#specContainerElement == null) throw new Error('No HTML element with id: ' + browseElementIds.specContainer);

        this.#selectElement.onchange = this.appCallback;
    }

    updateSpecs(newSpecs) {
        this.#specContainerElement.innerHTML = "";
        for (let newSpec of newSpecs) {
            let paragraph = document.createElement("p");
            paragraph.innerText = newSpec;
            this.#specContainerElement.appendChild(paragraph);
        }
    }

    init() {
        fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        .then(response => response.json())
        .then(komputers => {
            for(let komputer of komputers) {
                let option = document.createElement("option");
                option.value = JSON.stringify(komputer);
                option.innerText = komputer.title;
                this.#selectElement.append(option);
            }
            this.#selectElement.dispatchEvent(new Event("change"));
        })
        .catch(error => alert("Something terrible has happend!" + error.message));
    }

    update(state) {
        let { komputer } = state;
        if(typeof komputer != "undefined") {
            this.updateSpecs(komputer.specs);
        }
    }
}

export default Browse;