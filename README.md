# Assignment 4 - Kompture store

## Description
A webapp for working, saving and buying komputers.

## Live version
[Link to gitlab pages](https://vebsun95.gitlab.io/assignment-4/)

## Contributors
 - [Vebjørn Sundal](https://gitlab.com/vebsun95)

## Special thanks
to [Alvaro Montoro](https://alvaromontoro.com/blog/68002/creating-a-firework-effect-with-css) for providing the firework css code.

## Docs

This application tries to implement the MV~~C~~ and observer design patterns.

### Data flow
Note: In the application State is held in the class User.
![dataflow-diagram](./docs/assignment4_dataflow.png)
