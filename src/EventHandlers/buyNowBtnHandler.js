function buyNowBtnHandler(state)
{
    const currentBalance = state.balance;
    const laptopPrice = state.komputer.price;
    if(laptopPrice > currentBalance) {
        alert("Cant afford that computer");
        return;
    }
    return {
        balance: currentBalance - laptopPrice,
        showFireworks: true,
    }
}

export default buyNowBtnHandler;