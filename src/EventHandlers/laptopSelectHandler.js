function laptopSelectHandler(event) {
    return {
        komputer: JSON.parse(event.target.value),
    };
}

export default laptopSelectHandler;