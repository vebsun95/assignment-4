import browseSection from '../Components/Browse.js';

function fetchLaptops() {
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(komputers => {
        for(let komputer of komputers) {
            browseSection.addKomputer(komputer);
        }
        browseSection.triggerOnChangeEvent();
    })
    .catch(error => alert("Something terrible has happend!" + error.message));
}

export default fetchLaptops;