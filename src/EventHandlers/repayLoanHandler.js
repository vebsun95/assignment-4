import repayLoan from '../DomainLogic/repayLoan.js';

function repayLoanHandler(state) {
    let {loan, balance, salary} = state;
    [balance, loan, salary] = repayLoan(balance, loan, salary);
    return {
        balance,
        loan,
        salary
    };
}

export default repayLoanHandler;