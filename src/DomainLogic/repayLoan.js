function repayLoan(currentBalance, currentLoan, currentSalary) {
    if(currentLoan > currentSalary)
        return [currentBalance, currentLoan - currentSalary, 0];
    let remainingSalaryAfterPayingLoan = currentSalary - currentLoan;
    return [currentBalance + remainingSalaryAfterPayingLoan, 0, 0];
}

export default repayLoan;