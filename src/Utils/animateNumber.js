const ANIMATION_STEPS = 60;

const calcIncrement = (start, stop) => Math.round((stop - start) / ANIMATION_STEPS, 3);

function animateNumber(el, start, stop) {
    let increment = calcIncrement(start, stop);
    let counter = 0;
    const cb = () => {
        el.innerText = start + (increment * counter);
        counter++;
        if(counter <= ANIMATION_STEPS)
            window.requestAnimationFrame(cb);
        else
            el.innerText = stop;
    }
    window.requestAnimationFrame(cb)
}

export default animateNumber;