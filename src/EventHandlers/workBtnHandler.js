import doWork from "../DomainLogic/doWork.js";

function workBtnHandler(state)
{
    const currentSalary = state.salary;
    const newSalary = doWork(currentSalary);
    return { salary: newSalary};
}

export default workBtnHandler;