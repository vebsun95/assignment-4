import Component from "../Core/Component.js";
import animateNumber from "../Utils/animateNumber.js";

export const bankElementIds = {
    loanBtn: "get-a-loan-btn",
    balanceSpan: 'bank-balance-span',
    loanElement: 'bank-loan',
    loanSpan: 'bank-loan-span',
};

class Bank extends Component{
        #loanBtnElement = document.getElementById(bankElementIds.loanBtn);
        #balanceElement = document.getElementById(bankElementIds.balanceSpan);
        #loanElement = document.getElementById(bankElementIds.loanElement);
        #loanSpanElement = document.getElementById(bankElementIds.loanSpan);

        constructor(appCallback) {
            super(appCallback);

            if(this.#loanBtnElement == null) throw new Error('No HTML element with id: ' + bankElementIds.loanBtn);
            if(this.#balanceElement == null) throw new Error('No HTML element with id: ' + bankElementIds.balanceSpan);
            if(this.#loanElement == null) throw new Error('No HTML element with id: ' + bankElementIds.loanElement);
            if(this.#loanSpanElement == null) throw new Error('No HTML element with id: ' + bankElementIds.loanSpan);

            this.#loanBtnElement.onclick = this.appCallback;
        }

        init() {

        }

        update(state) {
            let {loan, balance, oldLoan, oldBalance} = state;
            animateNumber(this.#loanSpanElement, oldLoan, loan);
            animateNumber(this.#balanceElement, oldBalance, balance);
            if(loan < 1) {
                this.#loanElement.style.display = "none";
                this.#loanBtnElement.style.display = "unset";
            }
            else {
                this.#loanElement.style.display = "unset";
                this.#loanBtnElement.style.display = "none";
            }
        }
}


export default Bank;