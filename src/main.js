import Bank from "./Components/Bank.js";
import Work from "./Components/Work.js";
import Browse from './Components/Browse.js';
import Laptop from './Components/Laptop.js';
import User from "./Models/User.js";
import Fireworks from "./Components/Fireworks.js";

function main() {
    new Main().init().render();
}

class Main {
    components = [];
    root = document.getElementById("app");
    user = new User();

    constructor() {
        this.appCallback = this.appCallback.bind(this);
        this.components.push(
            new Bank(this.appCallback),
            new Browse(this.appCallback),
            new Laptop(this.appCallback),
            new Work(this.appCallback),
            new Fireworks(this.appCallback),
        );
        this.components.forEach(comp => {this.user.subscribe(comp)});
    }

    appCallback(event) {
        this.user.eventHandler(event);
    }

    init() {
        this.components.forEach(comp => {comp.init()});
        return this; // inorder to chain functions.
    }

    render() {
    }
}

window.onload = main;