import { workElementIds } from '../Components/Work.js';
import Subject from '../Core/Subject.js';
import workBtnHandler from '../EventHandlers/workBtnHandler.js';
import bankBtnHandler from '../EventHandlers/bankBtnHandler.js';
import buyNowBtnHandler from '../EventHandlers/buyNowBtnHandler.js';
import getALoanBtnHandler from '../EventHandlers/getALoanBtnHandler.js';
import { LaptopElementIds } from '../Components/Laptop.js';
import { bankElementIds } from '../Components/Bank.js';
import { browseElementIds } from '../Components/Browse.js';
import laptopSelectHandler from '../EventHandlers/laptopSelectHandler.js';
import repayLoanHandler from '../EventHandlers/repayLoanHandler.js';
import closeModalHandler from '../EventHandlers/closeModalHandler.js';
import { fireworkElementIds } from '../Components/Fireworks.js';

class User extends Subject {
    state = {
        salary: 0,
        loan: 0,
        balance: 500,
    }

    constructor() {
        super();
    }

    update(newState = {}) {
        let oldState = {
            oldSalary: this.state.salary,
            oldLoan  : this.state.loan,
            oldBalance:this.state.balance,
        }
        this.state = Object.assign(this.state, newState);
        this.notify({
            ...oldState,
            ...this.state,
        });
    }

    eventHandler(event) {
        if(eventHandlers.has(event.target.id)) {
            let handler = eventHandlers.get(event.target.id);
            let newState = handler(this.state);
            this.update(newState);
        }
        else if(event.target.id === browseElementIds.selectElement) {
            this.update(laptopSelectHandler(event));
        }
        else console.error("Could not find a handler for event: " + event.target.id);
    }
}

const eventHandlers = new Map([
    [workElementIds.workBtn, workBtnHandler],
    [workElementIds.bankBtn, bankBtnHandler],
    [workElementIds.repayBtn, repayLoanHandler],
    [LaptopElementIds.buyBtn, buyNowBtnHandler],
    [bankElementIds.loanBtn, getALoanBtnHandler],
    [fireworkElementIds.modalBg, closeModalHandler],
]);

export default User