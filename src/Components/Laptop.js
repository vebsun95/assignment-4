import Component from "../Core/Component.js";

export const LaptopElementIds = {
    img: 'laptop-img',
    title: 'laptop-name-header',
    description: 'laptop-description',
    price: 'laptop-price-span',
    buyBtn: 'buy-now-btn',
};

class Laptop extends Component {
        imgElement = document.getElementById(LaptopElementIds.img);
        #titleElement = document.getElementById(LaptopElementIds.title);
        #descriptionElement = document.getElementById(LaptopElementIds.description);
        #priceSpanElement = document.getElementById(LaptopElementIds.price);
        #buyBtnElement = document.getElementById(LaptopElementIds.buyBtn);

        constructor(appCallback) {
            super(appCallback);

            if(this.imgElement == null) throw new Error('No HTML element with id: ' + LaptopElementIds.img);
            if(this.#titleElement == null) throw new Error('No HTML element with id: ' + LaptopElementIds.title);
            if(this.#descriptionElement == null) throw new Error('No HTML element with id: ' + LaptopElementIds.description);
            if(this.#priceSpanElement == null) throw new Error('No HTML element with id: ' + LaptopElementIds.price);
            if(this.#buyBtnElement == null) throw new Error('No HTML element with id: ' + LaptopElementIds.buyBtn);

            this.#buyBtnElement.onclick = this.appCallback;
            this.imgElement.onerror = () => {this.imgErrorHandler()};

        }

        imgErrorHandler() {
            this.imgElement.src = "https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg";
        }

        update(state) {
            let { komputer } = state;

            this.#titleElement.innerText = komputer.title;
            this.#descriptionElement.innerText = komputer.description;
            this.#priceSpanElement.innerText = komputer.price;
            this.imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + komputer.image;
        }
}


export default Laptop;