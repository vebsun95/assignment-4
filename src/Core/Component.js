class Component {

    appCallback;

    constructor(appCallback) {
        this.appCallback = appCallback;
    };

    init() {

    };

    update(state) {

    };
}

export default Component;